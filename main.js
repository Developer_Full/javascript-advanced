var Product = function(params){
  this.nameProduct = params.nameProduct;
  this.priceProduct = params.priceProduct;
  this.urlProduct = params.urlProduct;
  Product.prototype.aboutProduct = function(){
    console.log('Названия продукта - ' + this.nameProduct);
    console.log('Цена продукта - ' + this.priceProduct);
    console.log('URL продукта - ' + this.urlProduct);
  }
};

var SaleProduct = function(params){
  Product.apply(this,arguments);
  this.newprice = params.newprice;
}

SaleProduct.prototype = Object.create(Product.prototype);
SaleProduct.prototype.constructor = SaleProduct;

SaleProduct.prototype.aboutProduct = function () {
  console.log('Названия продукта - ' + this.nameProduct);
  console.log('Старая Цена продукта - ' + this.priceProduct);
  console.log('URL продукта - ' + this.urlProduct);
  console.log('Новая цена продукта - ' + this.newprice);
}

var product1 =  new Product({
  nameProduct : 'notebook',
  priceProduct : '6500',
  urlProduct : 'to/notebook'
});

var product2 = new Product ({
  nameProduct : 'smartphone',
  priceProduct : '7800',
  urlProduct : 'to/smartphone'
});

var product3 = new Product ({
  nameProduct : 'iphone',
  priceProduct : '22000',
  urlProduct : 'to/iphone'
});


var saleProduct01 = new SaleProduct({
  nameProduct : 'lenovo',
  priceProduct : '4000',
  urlProduct : 'to/saleproduct/lenovo',
  newprice : '3300'
})

var saleProduct02 = new SaleProduct({
  nameProduct : 'huawai',
  priceProduct : '5000',
  urlProduct : 'to/saleproduct/huawai',
  newprice : '4500'
})

var saleProduct03 = new SaleProduct({
  nameProduct : 'Dell Monitor',
  priceProduct : '13000',
  urlProduct : 'to/saleproduct/dell',
  newprice : '9200'
})


product1.aboutProduct();
console.log("----------------------------------------")
product2.aboutProduct();
console.log("----------------------------------------")
product3.aboutProduct();
console.log("----------------------------------------")


console.log("*********************************** Информация по новой категории продуктов *****************************8")


saleProduct01.aboutProduct();
console.log('----------------------------------');
saleProduct02.aboutProduct();
console.log('----------------------------------');
saleProduct03.aboutProduct();
console.log('----------------------------------');