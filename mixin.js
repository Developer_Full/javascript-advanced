// ***************************** Миксины *************************
// Миксины требуются когда имеются несколько класов которые разлечаются по логике что они делают и не связаны с собой

var extend = function(target){
  if(!arguments[1]){
    return;
  }

  for(var i = 1 ; i < arguments.length; i++){
    var source = arguments[i]
    console.log(source);

    for(var prop in source){
      if(!target[prop] && source.hasOwnProperty(prop)){
        target[prop] = source[prop];
      }
    }
  }

}

var Product = function(name) {
  this.name = name;
}


var ProductList = function(name){
  this.name = name;
}

var nameMixin = {
  getName : function(){
    return this.name;
  }
};

var buyMixin = {
  buy : function() {
    console.log('Закупили след. - ' + this.name);
  }
};


extend(Product.prototype, nameMixin, buyMixin);
extend(ProductList.prototype, nameMixin, buyMixin);

var productbuy = new Product('Iphone');
var listbuy = new ProductList('Продукция Samsung');

console.log(productbuy)
console.log(listbuy)


console.log(productbuy.getName())
console.log(listbuy.getName())

productbuy.buy();
listbuy.buy();